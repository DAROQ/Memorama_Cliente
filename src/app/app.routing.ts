import { Routes } from '@angular/router';
import { MemoramaComponent } from './componentes/memorama/memorama.component';

export const rutasApp: Routes = [
    { path: 'memorama', component: MemoramaComponent },
    { path: '', redirectTo: '/memorama', pathMatch: 'full' }
];